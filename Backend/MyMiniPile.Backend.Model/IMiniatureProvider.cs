﻿using MyMiniPile.Backend.Model.Model;

namespace MyMiniPile.Backend.Model;

public interface IMiniatureProvider
{
    IEnumerable<Faction> GetAllFactions();

    bool TryGetFactionById(int factionId, out Faction? faction);

    IEnumerable<MiniatureType> GetAllMiniatureTypes();

    bool TryGetMiniatureTypeById(int miniatureTypeId, out MiniatureType? miniatureType);

    IEnumerable<Miniature> GetAllMiniatures();

    bool TryGetMiniatureById(int miniatureId, out Miniature? miniature);
}
