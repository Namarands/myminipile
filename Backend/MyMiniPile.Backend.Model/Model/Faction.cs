﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MyMiniPile.Backend.Model.Model;

public class Faction
{
    public int FactionId { get; init; }
    public string FactionName { get; init; }

    public Faction(int factionId, string factionName)
    {
        FactionId = factionId;
        FactionName = factionName;
    }
}

public static class FactionConfiguration
{
    public static void ConfigureFaction(this EntityTypeBuilder<Faction> builder)
    {
        builder.HasKey(faction => faction.FactionId);
        builder.Property(faction => faction.FactionName).IsRequired();
    }
}
