﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MyMiniPile.Backend.Model.Model;

public class MiniatureType
{
    public int MiniatureTypeId { get; init; }
    public int FactionId { get; init; }
    public string MiniatureName { get; init; }

    public MiniatureType(int miniatureTypeId, int factionId, string miniatureName)
    {
        MiniatureTypeId = miniatureTypeId;
        FactionId = factionId;
        MiniatureName = miniatureName;
    }
}

public static class MiniatureTypeConfiguration
{
    public static void ConfigureMiniatureType(this EntityTypeBuilder<MiniatureType> builder)
    {
        builder.HasKey(miniatureType => miniatureType.MiniatureTypeId);
        builder.Property(miniatureType => miniatureType.FactionId).IsRequired();
        builder.Property(miniatureType => miniatureType.MiniatureName).IsRequired();
    }
}
