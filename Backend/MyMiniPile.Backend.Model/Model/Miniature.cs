﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MyMiniPile.Backend.Model.Model;

public class Miniature
{
    public int MiniatureId { get; init; }
    public int MiniatureTypeId { get; init; }

    public Miniature(int miniatureId, int miniatureTypeId)
    {
        MiniatureId = miniatureId;
        MiniatureTypeId = miniatureTypeId;
    }
}

public static class MiniatureConfiguration
{
    public static void ConfigureMiniature(this EntityTypeBuilder<Miniature> builder)
    {
        builder.HasKey(miniature => miniature.MiniatureId);
        builder.Property(miniature => miniature.MiniatureTypeId).IsRequired();
    }
}
