﻿using Microsoft.EntityFrameworkCore;
using MyMiniPile.Backend.Model;
using MyMiniPile.Backend.Model.Model;

namespace MyMiniPile.Backend.DataProvider.PostgreSql;

public class PostgreSqlDbContext : DbContext, IMiniatureProvider
{
    private DbSet<Faction> Factions { get; set; }
    private DbSet<MiniatureType> MiniatureTypes { get; set; }
    private DbSet<Miniature> Miniatures { get; set; }

    public IEnumerable<Faction> GetAllFactions() => Factions;

    public IEnumerable<Miniature> GetAllMiniatures() => Miniatures;

    public IEnumerable<MiniatureType> GetAllMiniatureTypes() => MiniatureTypes;

    public bool TryGetFactionById(int factionId, out Faction? faction)
    {
        faction = Factions.FirstOrDefault(f => f.FactionId == factionId);
        return faction != null ;
    }

    public bool TryGetMiniatureById(int miniatureId, out Miniature? miniature)
    {
        miniature = Miniatures.FirstOrDefault(m => m.MiniatureId == miniatureId);
        return miniature != null;
    }

    public bool TryGetMiniatureTypeById(int miniatureTypeId, out MiniatureType? miniatureType)
    {
        miniatureType = MiniatureTypes.FirstOrDefault(m => m.MiniatureTypeId == miniatureTypeId);
        return miniatureType != null;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=postgres;Username=Namarand;Password=abc");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Faction>().ConfigureFaction();
        modelBuilder.Entity<MiniatureType>().ConfigureMiniatureType();
        modelBuilder.Entity<Miniature>().ConfigureMiniature();
    }
}
